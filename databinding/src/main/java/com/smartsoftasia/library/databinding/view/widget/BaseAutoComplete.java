package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.AutoCompleteTextView;

import com.smartsoftasia.library.databinding.helper.FontHelper;

/**
 * Created by Nott on 12/23/2015 AD.
 * TypedAutoComplete
 */
public class BaseAutoComplete extends AppCompatAutoCompleteTextView implements FontHelper.CustomFontWidget {

  public static final String TAG = "TypedAutoComplete";
  private FontHelper.FontStyle mCurrentStyle;

  public BaseAutoComplete(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseAutoComplete(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }


  private void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }

    FontHelper.setTypeFace(this, attrs);
  }

  // this is how to disable AutoCompleteTextView filter
  @Override
  protected void performFiltering(final CharSequence text, final int keyCode) {
    String filterText = "";
    super.performFiltering(filterText, keyCode);
  }

  @Override
  public void setCustomTypeface(Typeface typeface, float fontSizePercent) {
    setTypeface(typeface, getTypeface().getStyle());
    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * fontSizePercent);
  }
}
