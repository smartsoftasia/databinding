package com.smartsoftasia.library.databinding.view.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by androiddev01 on 12/3/2015 AD.
 */
public abstract class BaseDialogFragment extends SSADialogFragment {

  public static final String TAG = "BaseDialogFragment";

  public BaseDialogFragment() {

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    beforeLoadContentView(savedInstanceState);
    View root = inflater.inflate(getLayoutRes(), container, false);
    afterLoadContentView(savedInstanceState);

    return root;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setHasOptionsMenu(true);
    this.initialize();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  protected abstract int getLayoutRes();

  protected void beforeLoadContentView(Bundle savedInstanceState) {

  }

  protected void afterLoadContentView(Bundle savedInstanceState) {

  }

  protected void initialize() {

  }
}
