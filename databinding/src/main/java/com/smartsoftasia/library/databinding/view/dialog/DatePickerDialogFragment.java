package com.smartsoftasia.library.databinding.view.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by androiddev01 on 1/8/2016 AD.
 */
public class DatePickerDialogFragment extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

  private OnDateSelect mOnDateSelectListener;

  private Date mInitDate;
  private Date mMinDate;
  private Date mMaxDate;

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    // Use the current date as the default date in the picker
    final Calendar c = Calendar.getInstance();
    if (mInitDate!=null) c.setTime(mInitDate);
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    int day = c.get(Calendar.DAY_OF_MONTH);

    // Create a new instance of DatePickerDialog and return it
    DatePickerDialog dialog =  new DatePickerDialog(getActivity(), this, year, month, day);
    if (mMinDate!=null) dialog.getDatePicker().setMinDate(mMinDate.getTime());
    if (mMaxDate!=null)dialog.getDatePicker().setMaxDate(mMaxDate.getTime());
    return dialog;
  }

  public void onDateSet(DatePicker view, int year, int month, int day) {
    // Do something with the date chosen by the user
    if (mOnDateSelectListener!=null){
      mOnDateSelectListener.onDateSelect(new GregorianCalendar(year, month, day).getTime());
    }
  }

  public DatePickerDialogFragment setInitDate(Date initDate) {
    this.mInitDate = initDate;
    return this;
  }

  public DatePickerDialogFragment setMinDate(Date minDate) {
    mMinDate = minDate;
    return this;
  }

  public DatePickerDialogFragment setMaxDate(Date maxDate) {
    this.mMaxDate = maxDate;
    return this;
  }

  public DatePickerDialogFragment setOnDateSelectListener(OnDateSelect onDateSelectListener) {
    mOnDateSelectListener = onDateSelectListener;
    return this;
  }

  public interface OnDateSelect{
    void onDateSelect(Date date);
  }
}
