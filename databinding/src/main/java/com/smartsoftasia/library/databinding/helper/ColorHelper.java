package com.smartsoftasia.library.databinding.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import com.smartsoftasia.library.databinding.R;

/**
 * Created by Tar on 2/18/16.
 */
public class ColorHelper {

  private static Context context;

  public interface ImageTintColor {
    public void setImageTintColor(int color);
  }

  public static void init(Context _context) {
    context = _context;
  }

  public static void applyColor(View view, AttributeSet attrs) {
    ColorAttributes ca = new ColorAttributes(context, attrs);

    if (ca.shouldTintImage() && view instanceof ImageTintColor) {
      ((ImageTintColor) view).setImageTintColor(ca.imageTintColor);
      view.invalidate();
    }
  }

  public static class ColorAttributes {

    int imageTintColor;

    public ColorAttributes(Context context, AttributeSet attrs) {
      TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ViewColorHelper);

      imageTintColor = a.getColor(R.styleable.ViewColorHelper_imageTintColor, Color.TRANSPARENT);

      a.recycle();
    }

    protected boolean shouldTintImage() {
      return imageTintColor != Color.TRANSPARENT;
    }
  }
}
