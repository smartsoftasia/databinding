package com.smartsoftasia.library.databinding.helper;

/**
 * Created by SSA on 11/7/2016 AD.
 * Java, there is a very clear distinction between primitive and non-primitive types.
 *
 * A variable of a primitive type directly contains the value of that type (in other words, they
 * are
 * value types).
 *
 * A variable of a non-primitive type doesn't contain the value directly; instead, it is a
 * reference
 * (similar to a pointer) to an object. (It is not possible in Java to create user-defined value
 * types).
 *
 * Java has eight primitive types: byte, short, int, long, char, boolean, float and double.
 * Anything
 * else is a non-primitive type.
 *
 * See {@link <a href="http://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html>Primitive
 * Data Types</a>}
 */
public class PrimitiveCoverter {
  public static final String TAG = "PrimitiveCoverter";

  /**
   * Primitive Int
   *
   * @param i integer input
   * @return Primitive Int
   */
  public static int toPrimitive(Integer i) {
    return Validator.isValid(i) ? i : 0;
  }

  /**
   * Primitive Boolean
   *
   * @param b integer input
   * @return Primitive Boolean
   */
  public static boolean toPrimitive(Boolean b) {
    return Validator.isValid(b) ? b : false;
  }

  /**
   * Primitive Long
   *
   * @param l integer input
   * @return Primitive Long
   */
  public static long toPrimitive(Long l) {
    return Validator.isValid(l) ? l : 0;
  }

  /**
   * Primitive Float
   *
   * @param f integer input
   * @return Primitive Float
   */
  public static float toPrimitive(Float f) {
    return Validator.isValid(f) ? f : 0;
  }

  /**
   * Primitive Double
   *
   * @param d integer input
   * @return Primitive Double
   */
  public static double toPrimitive(Double d) {
    return Validator.isValid(d) ? d : 0;
  }

  /**
   * For get String non null & can't be empty
   *
   * @param s string input
   * @return String with non null & can't be empty
   */
  public static String notNullAllowEmpty(String s) {
    return Validator.isValidOrEmpty(s) ? s : "";
  }

  /**
   * For get String non null
   *
   * @param s string input
   * @return String with non null
   */
  public static String notNull(String s) {
    return Validator.isValid(s) ? s : "";
  }

  /**
   * For get Int-String
   *
   * @param s string input
   * @return String Int-String
   */
  public static String stringToIntString(String s) {
    return Validator.isValid(s) ? s : "0";
  }

  /**
   * For get Int from String
   *
   * @param s string input
   * @return Int
   */
  public static int stringToInt(String s) {
    return Validator.isValid(s) ? Integer.valueOf(s) : 0;
  }

  /**
   * Get Standard URL
   *
   * @param url string input
   * @return Standard URL
   */
  public static String toStandartUrl(String url) {
    if (url != null) {
      if (url.startsWith("//")) {
        url = "http:" + url;
      } else if (!url.startsWith("http://") && !url.startsWith("https://")) {
        url = "http://" + url;
      }
    } else {
      url = "http://";
    }
    return url;
  }
}
