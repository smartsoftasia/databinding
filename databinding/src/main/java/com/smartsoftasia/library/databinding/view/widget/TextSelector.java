package com.smartsoftasia.library.databinding.view.widget;

/**
 * @author Gregoire Barret
 *         On 8/16/2016 AD
 *         For TeeTooEee.
 */

public interface TextSelector {

  CharSequence getSelectedText();

  CharSequence getLoadingText();

  CharSequence getText(boolean selected);

  CharSequence getUnselectedText();
}

