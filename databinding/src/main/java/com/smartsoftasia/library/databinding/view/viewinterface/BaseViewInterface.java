package com.smartsoftasia.library.databinding.view.viewinterface;


/**
 * Created by androiddev01 on 11/30/2015 AD.
 */
public interface BaseViewInterface {
  void onError(String error);
  void onError(Throwable error);
}
