package com.smartsoftasia.library.databinding.domain.controller;

import android.content.Context;

/**
 * Created by Tar on 3/17/16 AD.
 */
public abstract class BaseSharedPreference {

  public static final String TAG = "BaseSharePreference";

  protected Context mContext;

  public BaseSharedPreference(Context context) {
    mContext = context;
  }

}
