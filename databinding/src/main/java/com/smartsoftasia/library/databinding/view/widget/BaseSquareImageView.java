package com.smartsoftasia.library.databinding.view.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by androiddev01 on 4/20/2016 AD.
 */
public class BaseSquareImageView extends BaseImageView {
  public static final String TAG = "BaseSquareImageView";

  public BaseSquareImageView(Context context) {
    super(context);
  }

  public BaseSquareImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    int measuredWidth = getMeasuredWidth();

    setMeasuredDimension(measuredWidth, measuredWidth);

  }
}
