package com.smartsoftasia.library.databinding.helper;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by androiddev01 on 4/26/2016 AD.
 */
public class CountryCodeHelper {

  private ArrayList<CountryCode> countryCodes = new ArrayList<>();

  @Inject
  public CountryCodeHelper(Context context) {
    readCountriesData(context);
  }

  private void readCountriesData(Context context) {
    countryCodes.clear();

    int fileId = context.getResources().getIdentifier("countries", "raw", context.getPackageName());
    InputStream is = context.getResources().openRawResource(fileId);
    InputStreamReader isr = new InputStreamReader(is);
    BufferedReader br = new BufferedReader(isr);

    String inputLine = new String();
    StringBuffer sb = new StringBuffer();

    try {
      while ((inputLine = br.readLine()) != null) {
        sb.append(inputLine);
        sb.append("\n");
      }

      br.close();

      String json = sb.toString();
      JSONArray countries = new JSONArray(json);

      for (int i = 0; i < countries.length(); i++) {
        countryCodes.add(new CountryCode(countries.getJSONObject(i)));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public ArrayList<CountryCode> getCountryCodes() {
    return countryCodes;
  }

  public static class CountryCode {
    public String name;
    public String dialCode;
    public String ISOCode;

    public CountryCode(JSONObject jsonObject) {
      name = jsonObject.optString("name");
      dialCode = jsonObject.optString("dial_code");
      ISOCode = jsonObject.optString("code");
    }

    @Override
    public String toString() {
      return dialCode;
    }
  }
}
