package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.smartsoftasia.library.databinding.view.adapter.BaseLinearAdapter;


/**
 * Created by androiddev01 on 12/21/2015 AD.
 */
public class LinearListView extends LinearLayout implements BaseLinearAdapter.BaseLinearAdapterInterface {
  public static final String TAG = "StaticListView";

  private BaseLinearAdapter mAdapter;

  public LinearListView(Context context) {
    super(context);
  }

  public LinearListView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public LinearListView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public LinearListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  public void setAdapter(BaseLinearAdapter adapter) {
    mAdapter = adapter;
    mAdapter.setAdapterInterface(this);
  }

  @Override
  public void addItem(int position) {
    addView(mAdapter.getView(position,null, this));

  }

  @Override
  public void addItems() {
    removeAllViews();
    for (int i = 0; i < mAdapter.getCount(); i++) {
      addView(mAdapter.getView(i,null, this), i);
    }
  }

  @Override
  public void removeItem(int position) {
  removeViewAt(position);
  }

  @Override
  public void deleteItems() {
    removeAllViews();
  }
}
