package com.smartsoftasia.library.databinding.view.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class NumberPickerDialog extends DialogFragment {
  public static final String TAG = "NumberPickerDialog";

  public interface OnNumberPickerListener {
    void numberSelected(int i);
  }

  private String title, cancel, ok;
  private int maxValue;
  private int minValue;
  private OnNumberPickerListener mListener;

  public static NumberPickerDialog newInstance() {
    NumberPickerDialog fragment = new NumberPickerDialog();
    Bundle arg = new Bundle();
    fragment.setArguments(arg);
    return fragment;
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    RelativeLayout linearLayout = new RelativeLayout(getContext());
    final NumberPicker aNumberPicker = new NumberPicker(getContext());
    aNumberPicker.setMaxValue(maxValue);
    aNumberPicker.setMinValue(minValue);

    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
    RelativeLayout.LayoutParams numPickerParams = new RelativeLayout.LayoutParams(
        RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    numPickerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

    linearLayout.setLayoutParams(params);
    linearLayout.addView(aNumberPicker, numPickerParams);

    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
    alertDialogBuilder.setTitle(title);
    alertDialogBuilder.setView(linearLayout);
    alertDialogBuilder.setCancelable(false)
        .setPositiveButton(ok, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            if (mListener != null) {
              mListener.numberSelected(aNumberPicker.getValue());
            }
          }
        })
        .setNegativeButton(cancel, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
          }
        });
    return alertDialogBuilder.create();
  }

  public NumberPickerDialog setCancelTextButton(String cancelButton) {
    this.cancel = cancelButton;
    return this;
  }

  public NumberPickerDialog setOkTextButton(String okButton) {
    this.ok = okButton;
    return this;
  }

  public NumberPickerDialog setTitleDialog(String titleText) {
    this.title = titleText;
    return this;
  }

  public NumberPickerDialog setMaxValue(int maxValue) {
    this.maxValue = maxValue;
    return this;
  }

  public NumberPickerDialog setMinValue(int minValue) {
    this.minValue = minValue;
    return this;
  }

  public void setListener(OnNumberPickerListener listener) {
    this.mListener = listener;
  }
}
