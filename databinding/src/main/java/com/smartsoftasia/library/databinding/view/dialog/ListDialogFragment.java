package com.smartsoftasia.library.databinding.view.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.smartsoftasia.library.databinding.view.adapter.BaseLinearAdapter;


/**
 * @author Gregoire Barret
 *         On 6/24/2016 AD
 *         For TeeTooEee.
 */
public class ListDialogFragment extends DialogFragment {
  public static final String TAG = "ListDialogFragment";

  interface OnItemSelectListener{
    void onItemSelected(int position);
  }

  private String[] mArray;
  private BaseLinearAdapter.OnItemClickListener mOnItemClickListener;

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder
        .setItems(mArray, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            if (mOnItemClickListener != null){
              mOnItemClickListener.onItemClick(which);
            }
          }
        });
    return builder.create();
  }

  public ListDialogFragment setArray(String[] array) {
    mArray = array;
    return this;
  }

  public ListDialogFragment setOnItemClickListener(BaseLinearAdapter.OnItemClickListener onItemClickListener) {
    mOnItemClickListener = onItemClickListener;
    return this;
  }
}
