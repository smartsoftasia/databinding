package com.smartsoftasia.library.databinding.helper;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Nott on 6/6/2016 AD.
 * Android core module
 */
public class NotificationHelper {
  public static final String TAG = "NotificationHelper";

  /**
   * Cancel notification has been notified.
   *
   * @param context context
   * @param icon iconId
   * @param title title of notification
   * @param text message of notification
   */
  public static void notification(Context context, int icon, String title, String text) {
    int notificationID = 0;

    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
    mBuilder.setSmallIcon(icon);
    mBuilder.setContentTitle(title);
    mBuilder.setContentText(text);

    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(
        Context.NOTIFICATION_SERVICE);
    mNotificationManager.notify(notificationID, mBuilder.build());
  }

  /**
   * Cancel notification has been notified.
   *
   * @param context context
   * @param notifyId notificationId
   */
  public static void cancelNotification(Context context, int notifyId) {
    String notificationService = Context.NOTIFICATION_SERVICE;
    NotificationManager notificationManager = (NotificationManager) context.getSystemService(
        notificationService);
    notificationManager.cancel(notifyId);
  }
}