package com.smartsoftasia.library.databinding.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Tar on 3/17/16 AD.
 * BaseFragment
 */
public abstract class BaseFragment extends SSAFragment {


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    beforeLoadContentView(savedInstanceState);
    View view = inflater.inflate(getViewResourceId(), container, false);
    afterLoadContentView(savedInstanceState);

    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setHasOptionsMenu(true);
    this.initialize();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  public abstract int getViewResourceId();

  protected void beforeLoadContentView(Bundle savedInstanceState) {

  }

  protected void afterLoadContentView(Bundle savedInstanceState) {

  }

  protected void initialize() {

  }
}
