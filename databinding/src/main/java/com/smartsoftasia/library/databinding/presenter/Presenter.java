package com.smartsoftasia.library.databinding.presenter;

public interface Presenter {

  void create();

  void start();

  void resume();

  void pause();

  void destroy();

  void restart();
}
