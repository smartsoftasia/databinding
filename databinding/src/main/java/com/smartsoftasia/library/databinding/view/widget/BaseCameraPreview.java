package com.smartsoftasia.library.databinding.view.widget;

import android.content.Context;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;
import java.util.List;

/**
 * Created by Tar on 5/18/16 AD.
 */
public abstract class BaseCameraPreview extends SurfaceView implements SurfaceHolder.Callback {
  public static final String TAG = "CameraPreview";
  private static  final int FOCUS_AREA_SIZE= 300;

  private SurfaceHolder mHolder;
  private Camera mCamera;
  private List<Camera.Size> mSupportedPreviewSizes;
  private Camera.Size mPreviewSize;

  public BaseCameraPreview(Context context, Camera camera) {
    super(context);
    mCamera = camera;
    mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

    mHolder = getHolder();
    mHolder.addCallback(this);
    mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        focus(event);
        return false;
      }
    });
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
    final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

    if (mSupportedPreviewSizes != null) {
      mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
    }

    float ratio;
    if (mPreviewSize.height >= mPreviewSize.width) {
      ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
    } else {
      ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;
    }

    if (width * ratio > height) {
      setMeasuredDimension(width, (int) (width * ratio));
    } else {
      setMeasuredDimension((int) (height / ratio), height);
    }
  }

  @Override
  public void surfaceCreated(SurfaceHolder holder) {
    try {
      mCamera.setPreviewDisplay(holder);
      mCamera.startPreview();
    } catch (IOException e) {
      Log.d(TAG, "Error setting camera preview: " + e.getMessage());
    }
  }

  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {

  }

  @Override
  public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    if (mHolder.getSurface() == null) {
      return;
    }

    try {
      mCamera.stopPreview();
    } catch (Exception e) {
      Log.d(TAG, "Error stoping camera preview: " + e.getMessage());
    }

    try {
      Camera.Parameters parameters = mCamera.getParameters();
      parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
      configureCameraParameter(parameters);
      mCamera.setParameters(parameters);

      // TODO : Support Landscape
      configureCamera(mCamera);
      mCamera.setPreviewDisplay(mHolder);
      mCamera.startPreview();
      mCamera.autoFocus(null);
    } catch (Exception e) {
      Log.d(TAG, "Error starting camera preview: " + e.getMessage());
    }
  }

  protected void focus(MotionEvent event) {

  }

  protected void configureCameraParameter(Camera.Parameters parameters) {
    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
  }

  protected void configureCamera(Camera camera) {
    mCamera.setDisplayOrientation(90);
  }

  protected Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
    final double ASPECT_TOLERANCE = 0.1;
    double targetRatio = (double) h / w;

    if (sizes == null)
      return null;

    Camera.Size optimalSize = null;
    double minDiff = Double.MAX_VALUE;

    int targetHeight = h;

    for (Camera.Size size : sizes) {
      double ratio = (double) size.height / size.width;
      if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
        continue;

      if (Math.abs(size.height - targetHeight) < minDiff) {
        optimalSize = size;
        minDiff = Math.abs(size.height - targetHeight);
      }
    }

    if (optimalSize == null) {
      minDiff = Double.MAX_VALUE;
      for (Camera.Size size : sizes) {
        if (Math.abs(size.height - targetHeight) < minDiff) {
          optimalSize = size;
          minDiff = Math.abs(size.height - targetHeight);
        }
      }
    }

    return optimalSize;
  }

  protected Rect calculateFocusArea(float x, float y) {
    int left = clamp(Float.valueOf((x / getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
    int top = clamp(Float.valueOf((y / getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

    return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
  }

  protected int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
    int result;
    if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
      if (touchCoordinateInCameraReper>0){
        result = 1000 - focusAreaSize/2;
      } else {
        result = -1000 + focusAreaSize/2;
      }
    } else{
      result = touchCoordinateInCameraReper - focusAreaSize/2;
    }
    return result;
  }
}
