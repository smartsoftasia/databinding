package com.smartsoftasia.library.databinding.viewModel;

/**
 * Created by Tar on 11/9/16.
 */

public interface BaseViewModelInterface {

  void onStart();

  void onResume();

  void onPause();

  void onStop();

  void onDestroy();

}
