package com.smartsoftasia.library.databinding.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartsoftasia.library.databinding.R;
import com.smartsoftasia.library.databinding.helper.Validator;
import com.smartsoftasia.library.databinding.view.widget.BaseEditText;
import com.smartsoftasia.library.databinding.view.widget.BaseTextView;

/**
 * Created by androiddev01 on 3/16/2016 AD.
 */
public class InputDialog extends BaseDialogFragment {
  public static final String TAG = "CancelDialog";

  public interface InputDialogListener {
    void onPositiveButtonClick(InputDialog dialog, String comment);

    void onNegativeButtonClick(InputDialog dialog);
  }

  protected BaseEditText editTextInput;
  protected BaseTextView textViewTitle;
  protected BaseTextView textViewContent;
  protected BaseTextView textViewCancelButton;
  protected BaseTextView textViewProceedButton;

  protected String title;
  protected String content;
  protected String hint;
  protected String error;
  protected String cancelButtonTitle;
  protected String proceedButtonTitle;

  protected boolean enableBlankInput = false;
  protected boolean dismissOnCancel = true;
  protected boolean dismissOnInput = true;
  protected boolean dismissOnError = false;

  private InputDialogListener mInputDialogListener;

  public static InputDialog newInstance() {
    InputDialog fragment = new InputDialog();
    Bundle arg = new Bundle();
    fragment.setArguments(arg);
    return fragment;
  }

  public InputDialog() {

  }

  @Override
  public int getLayoutRes() {
    return R.layout.dialog_input;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View root = super.onCreateView(inflater, container, savedInstanceState);

    editTextInput = (BaseEditText) root.findViewById(R.id.editText_comment);
    textViewTitle = (BaseTextView) root.findViewById(R.id.textView_title);
    textViewContent = (BaseTextView) root.findViewById(R.id.textView_content);
    textViewCancelButton = (BaseTextView) root.findViewById(R.id.button_cancel);
    textViewProceedButton = (BaseTextView) root.findViewById(R.id.button_ok);

    textViewCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onCancelClick(v);
      }
    });

    textViewProceedButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onOkClick(v);
      }
    });

    return root;
  }
  @Override
  public void onStart() {
    super.onStart();
    Dialog dialog = getDialog();

    if (dialog != null) {
      dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if (Validator.isValid(title)) {
      textViewTitle.setText(title);
    } else {
      textViewTitle.setVisibility(View.GONE);
    }

    if (Validator.isValid(content)) {
      textViewContent.setText(content);
    } else {
      textViewContent.setVisibility(View.GONE);
    }

    if (Validator.isValid(hint)) {
      editTextInput.setHint(hint);
    } else {
      textViewContent.setHint(null);
    }

    if (Validator.isValid(proceedButtonTitle)) {
      textViewProceedButton.setText(proceedButtonTitle);
    }

    if (Validator.isValid(cancelButtonTitle)) {
      textViewCancelButton.setText(cancelButtonTitle);
    }
  }

  public void onCancelClick(View v) {
    if (mInputDialogListener != null) {
      mInputDialogListener.onNegativeButtonClick(this);
    }

    if (dismissOnCancel) {
      this.dismiss();
    }
  }

  public void onOkClick(View v) {
    String input = editTextInput.getText().toString();

    if (!Validator.isValid(input) && !enableBlankInput) {
      if (Validator.isValid(error)) {
        editTextInput.setError(error);
      }

      if (dismissOnError) {
        this.dismiss();
      }
    } else {
      if (mInputDialogListener != null) {
        mInputDialogListener.onPositiveButtonClick(this, input);

        if (dismissOnInput) {
          this.dismiss();
        }
      }
    }
  }

  public InputDialog setInputDialogListener(InputDialogListener inputDialogListener) {
    mInputDialogListener = inputDialogListener;
    return this;
  }

  public InputDialog setTitle(String title) {
    this.title = title;

    return this;
  }

  public InputDialog setContent(String content) {
    this.content = content;

    return this;
  }

  public InputDialog setHint(String hint) {
    this.hint = hint;

    return this;
  }

  public InputDialog setError(String error) {
    this.error = error;

    return this;
  }

  public InputDialog setEnableBlankInput(boolean enableBlankInput) {
    this.enableBlankInput = enableBlankInput;

    return this;
  }

  public InputDialog setDismissOnCancel(boolean dismissOnCancel) {
    this.dismissOnCancel = dismissOnCancel;

    return this;
  }

  public InputDialog setDismissOnInput(boolean dismissOnInput) {
    this.dismissOnInput = dismissOnInput;

    return this;
  }

  public InputDialog setDismissOnError(boolean dismissOnError) {
    this.dismissOnError = dismissOnError;

    return this;
  }
}
