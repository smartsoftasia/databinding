package com.smartsoftasia.library.databinding.view.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by androiddev01 on 9/27/2016 AD.
 */

public class BaseSquareRoundedImageView extends BaseRoundedImageView {

  public BaseSquareRoundedImageView(Context context) {
    super(context);
  }

  public BaseSquareRoundedImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    int measuredWidth = getMeasuredWidth();

    setMeasuredDimension(measuredWidth, measuredWidth);

  }
}
