package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class BaseLinearLayout extends LinearLayout {

  public interface OnSizeChangedListener {

    public void onSizeChanged(int id, int newWidth, int newHeight, int oldWidth, int oldHeight);
  }

  private OnSizeChangedListener onSizeChangedListener = null;

  public BaseLinearLayout(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseLinearLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public BaseLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init(context, attrs, defStyleAttr);
  }

  public void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }
  }

  public void setWidth(double w) {
    getLayoutParams().width = (int) w;
  }

  public void setHeight(double h) {
    getLayoutParams().height = (int) h;
  }

  public void setSize(double w, double h) {
    getLayoutParams().width = (int) w;
    getLayoutParams().height = (int) h;
  }

  @Override
  protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
    super.onSizeChanged(xNew, yNew, xOld, yOld);

    if (onSizeChangedListener != null) {
      onSizeChangedListener.onSizeChanged(this.getId(), xNew, yNew, xOld, yOld);
    }
  }

  public void setOnSizeChangedListener(OnSizeChangedListener listener) {
    this.onSizeChangedListener = listener;
  }
}
