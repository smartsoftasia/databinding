package com.smartsoftasia.library.databinding.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageHelper {

  private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {
      final int halfHeight = height / 2;
      final int halfWidth = width / 2;

      while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }

    return inSampleSize;
  }

  public static Bitmap loadBitmap(File file) throws IOException {
    return loadBitmap(file, 0, 0);
  }

  public static Bitmap loadBitmap(File file, int reqWidth, int reqHeight) throws IOException {
    final BitmapFactory.Options options = new BitmapFactory.Options();

    if (reqHeight > 0 && reqWidth > 0) {
      options.inJustDecodeBounds = true;
      BitmapFactory.decodeFile(file.getPath(), options);

      options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
      options.inJustDecodeBounds = false;
    }

    Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
    ExifInterface exif = new ExifInterface(file.getPath());
    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

    switch (orientation) {
      case ExifInterface.ORIENTATION_ROTATE_90:
        return rotateBitmap(bitmap, 90);

      case ExifInterface.ORIENTATION_ROTATE_180:
        return rotateBitmap(bitmap, 180);
      case ExifInterface.ORIENTATION_ROTATE_270:
        return rotateBitmap(bitmap, 270);
      default:
        return bitmap;
    }
  }

	public static Bitmap rotateBitmap(Bitmap input, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		Bitmap output = Bitmap.createBitmap(input, 0, 0, input.getWidth(), input.getHeight(), matrix, true);

		// if (output != input && !input.isRecycled()) {
		// input.recycle();
		// }

		return output;
	}

	public static Bitmap cropSquareBitmap(Bitmap input) {
		return cropBitmap(input, 1);
	}

  public static Bitmap cropBitmap(Bitmap input, float widthByHeightRatio) {
    int w = input.getWidth();
    int h = input.getHeight();

    float wScale;
    float hScale;

    if (widthByHeightRatio > 0) {
      wScale = widthByHeightRatio;
      hScale = 1;
    } else if (widthByHeightRatio < 0) {
      wScale = 1;
      hScale = widthByHeightRatio;
    } else {
      if (w == h) {
        return input;
      }

      wScale = 1;
      hScale = 1;
    }

    float factor = Math.min(w / wScale, h / hScale);
    int nw = (int) (factor * wScale);
    int nh = (int) (factor * hScale);

    return ThumbnailUtils.extractThumbnail(input, nw, nh);
  }

	public static Bitmap scaleBitmap(Bitmap input, int width, int height, boolean keepRatio) {
		if (keepRatio) {
			float w = input.getWidth();
			float h = input.getHeight();
			float wRatio = width / w;
			float hRatio = height / h;

			float ratio;
			if (hRatio < wRatio) {
				ratio = hRatio;
			} else {
				ratio = wRatio;
			}

			width = (int) (ratio * w);
			height = (int) (ratio * h);
		}

		Bitmap output = Bitmap.createScaledBitmap(input, width, height, true);

		// if (output != input && !input.isRecycled()) {
		// input.recycle();
		// }

		return output;
	}

	public static Bitmap convertToARGB888(Bitmap input) {
		if (input.getConfig() == Config.ARGB_8888) {
			return input;
		}

		int w = input.getWidth();
		int h = input.getHeight();
		int totalPixels = w * h;
		int[] pixels = new int[totalPixels];

		input.getPixels(pixels, 0, w, 0, 0, w, h);
		Bitmap output = Bitmap.createBitmap(w, h, Config.ARGB_8888);

		output.setPixels(pixels, 0, w, 0, 0, w, h);

		// if (output != input && !input.isRecycled()) {
		// input.recycle();
		// }

		return output;
	}

	public static Bitmap blur(Context context, Drawable drawable, int radius) {
		Bitmap bitmap;

		if (drawable instanceof BitmapDrawable) {
			bitmap = ((BitmapDrawable) drawable).getBitmap();
		} else {
			bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
			drawable.draw(canvas);
		}

		return blur(context, bitmap, radius);
	}

	public static Bitmap blur(Context context, Bitmap input, int radius) {
		if (input == null || radius < 1 || radius > 25) {
			return input;
		}

		Bitmap scaled = scaleBitmap(input, 512, 512, true);
		Bitmap converted = convertToARGB888(scaled);

		Bitmap output = Bitmap.createBitmap(converted.getWidth(), converted.getHeight(), Config.ARGB_8888);

		return output;
	}

  public static void saveBitmapAsJPEG(File file, Bitmap bitmap) throws Exception {
    saveBitmap(file, bitmap, Bitmap.CompressFormat.JPEG, 100);
  }

  public static void saveBitmapAsPNG(File file, Bitmap bitmap) throws Exception {
    saveBitmap(file, bitmap, Bitmap.CompressFormat.PNG, 100);
  }

  public static void saveBitmap(File file, Bitmap bitmap, Bitmap.CompressFormat compressFormat, int compressLevel) throws Exception {
    BufferedOutputStream buf = new BufferedOutputStream(new FileOutputStream(file));
    bitmap.compress(compressFormat, compressLevel, buf);
    buf.flush();
    buf.close();
  }
}
