package com.smartsoftasia.library.databinding.bindingAdapter;

import android.databinding.BindingAdapter;
import android.support.v4.content.ContextCompat;

import com.smartsoftasia.library.databinding.view.widget.BaseEditText;

/**
 * Created by Tar on 11/16/16.
 */

public class EditTextBindingAdapter {

  @BindingAdapter("android:textColor")
  public static void setTextColor(BaseEditText baseEditText, int colorResource) {
    if (colorResource == 0) {
      return;
    }

    baseEditText.setTextColor(ContextCompat.getColor(baseEditText.getContext(), colorResource));
  }

  @BindingAdapter("android:text")
  public static void setText(BaseEditText baseEditText, int textResource) {
    baseEditText.setText(textResource);
  }

  @BindingAdapter("android:text")
  public static void setText(BaseEditText baseEditText, String text) {
    baseEditText.setText(text);
  }
}
