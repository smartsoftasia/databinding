package com.smartsoftasia.library.databinding.dependency.module;

import android.content.Context;


import com.smartsoftasia.library.databinding.domain.executor.JobExecutor;
import com.smartsoftasia.library.databinding.domain.executor.PostExecutionThread;
import com.smartsoftasia.library.databinding.domain.executor.ThreadExecutor;
import com.smartsoftasia.library.databinding.domain.executor.UIThread;
import com.smartsoftasia.library.databinding.domain.executor.UiTheadHandlerImpl;
import com.smartsoftasia.library.databinding.domain.executor.UiThreadHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ThreadModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    UiThreadHandler provideUiThreadHandler(Context context) {
        return new UiTheadHandlerImpl(context);
    }
}
