package com.smartsoftasia.library.databinding.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by androiddev01 on 2/1/2016 AD.
 */
public class DateHelper {
  public static final String TAG = "DateHelper";

  public DateHelper() {
  }

  public static String dateTimeToString(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  public static String dateTimeToHourString(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("HH:mmaa", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  public static String dateTimeToDDMMMMYYYY(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  public static String dateTimeToDDMMYYYY(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  /**
   * Transform the date to string following the format
   * @param date
   * @param format
   * @return
   */
  public static String dateToString(Date date, String format) {
    SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }


  /**
   * GetCurrentTimeStamp
   * @return yyyy-MM-dd HH:mm:ss formate date as string
   */
  public static String getCurrentTimeStamp() {
    try {

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
      return dateFormat.format(new Date()); // Find today date
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
