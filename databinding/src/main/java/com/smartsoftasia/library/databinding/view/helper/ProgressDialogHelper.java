package com.smartsoftasia.library.databinding.view.helper;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by androiddev01 on 5/16/2016 AD.
 */
public class ProgressDialogHelper {
  public static final String TAG = "ProgressDialogHelper";

  public static ProgressDialog sProgressDialog;

  public static ProgressDialog show(Context context, String title, String message){
    if (sProgressDialog==null){
      sProgressDialog = ProgressDialog.show(context, title,
          "message", true);
    }
    return sProgressDialog;
  }

  public static void dismiss(){
    if (sProgressDialog != null){
      sProgressDialog.dismiss();
      sProgressDialog = null;
    }
  }
}
