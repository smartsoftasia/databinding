package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.smartsoftasia.library.databinding.helper.FontHelper;


/**
 * @author gregoire
 */
public class BaseEditText extends AppCompatEditText implements FontHelper.CustomFontWidget {

  public BaseEditText(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseEditText(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }


  private void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }

    FontHelper.setTypeFace(this, attrs);
  }

  @Override
  public void setCustomTypeface(Typeface typeface, float fontSizePercent) {
    int style = Typeface.NORMAL;

    if (getTypeface() != null) {
      style = getTypeface().getStyle();
    }

    setTypeface(typeface, style);
    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * fontSizePercent);
  }
}
