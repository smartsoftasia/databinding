package com.smartsoftasia.library.databinding.helper;

import android.text.Html;
import android.text.Spanned;

/**
 * Created by gregoire on 17/11/2559.
 * Compatible wrapper for Html class.
 */
public class HtmlCompact {

  @SuppressWarnings("deprecation")
  public static Spanned fromHtml(String source) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      return Html.fromHtml(source,Html.FROM_HTML_MODE_LEGACY);
    } else {
      return Html.fromHtml(source);
    }
  }

}
