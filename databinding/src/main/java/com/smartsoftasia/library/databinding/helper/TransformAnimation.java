package com.smartsoftasia.library.databinding.helper;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class TransformAnimation extends Animation {
	private View view;
	private int newWidth = -1;
	private int newHeight = -1;
	private int currentWidth;
	private int currentHeight;

	public TransformAnimation(View view) {
		this.view = view;
	}

	public TransformAnimation(View view, long duration) {
		this.view = view;
		this.setDuration(duration);
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		if (newWidth != -1) {
			view.getLayoutParams().width = (int) (currentWidth + ((newWidth - currentWidth) * interpolatedTime));
		}

		if (newHeight != -1) {
			view.getLayoutParams().height = (int) (currentHeight + ((newHeight - currentHeight) * interpolatedTime));
		}

		view.requestLayout();
	}

	@Override
	public boolean willChangeBounds() {
		return true;
	}

	public TransformAnimation setNewWidth(int newWidth) {
		this.newWidth = newWidth;
		this.currentWidth = view.getWidth();

		return this;
	}

	public TransformAnimation setNewHeight(int newHeight) {
		this.newHeight = newHeight;
		this.currentHeight = view.getHeight();

		return this;
	}

	public TransformAnimation setNewSize(int newWidth, int newHeight) {
		this.setNewWidth(newWidth);
		this.setNewHeight(newHeight);

		return this;
	}

	public void start() {
		if (view.getLayoutParams().height == 0 || view.getLayoutParams().width == 0 || view.getVisibility() != View.VISIBLE) {
			view.setVisibility(View.INVISIBLE);
		}

		view.startAnimation(this);
		view.setVisibility(View.VISIBLE);
	}
}
