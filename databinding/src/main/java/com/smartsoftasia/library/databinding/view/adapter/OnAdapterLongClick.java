package com.smartsoftasia.library.databinding.view.adapter;

import android.support.v7.widget.RecyclerView;

/**
 * Created by gregoire barret on 4/28/15.
 * For Perfumist project.
 */
public interface OnAdapterLongClick {
  void onAdapterLongClick(int position, Object item, RecyclerView.ViewHolder view);
}
