package com.smartsoftasia.library.databinding.helper;

import android.text.TextUtils;

import java.text.DecimalFormat;

/**
 * Created by Nott on 9/15/2016 AD.
 * Android core module
 */
public class NumberHelper {
  public static final String TAG = "NumberHelper";

  /**
   * @param number String value
   * @return Number String with comma
   */
  public static String getDecimalWithCommaFormatString(String number) {
    double temp;
    if (TextUtils.isEmpty(number)) {
      return "0";
    }

    try {
      temp = Double.parseDouble(number);
    } catch (NumberFormatException e) {
      return "0";
    }

    DecimalFormat format = new DecimalFormat();
    format.setDecimalSeparatorAlwaysShown(false);
    return format.format(temp);
  }
}
