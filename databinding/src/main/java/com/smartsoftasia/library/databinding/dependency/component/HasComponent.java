package com.smartsoftasia.library.databinding.dependency.component;

public interface HasComponent<C> {

    C getComponent();
}
