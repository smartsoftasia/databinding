package com.smartsoftasia.library.databinding.helper;

import android.content.Context;

import java.security.SecureRandom;

public class TranslationHelper {

	private static SecureRandom sr = null;
	private static Context context;

	public static void init(Context _context) {
		context = _context;
	}

	public static String get(int id) {
		return get(context, id);
	}

	public static String get(Context context, int id) {
		return context.getString(id);
	}

	public static String get(int id, Object... args) {
		return get(context, id, args);
	}

	public static String get(Context context, int id, Object... args) {
		return context.getString(id, args);
	}
}
