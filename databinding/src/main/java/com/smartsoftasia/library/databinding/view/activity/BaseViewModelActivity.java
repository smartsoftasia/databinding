package com.smartsoftasia.library.databinding.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.smartsoftasia.library.databinding.viewModel.BaseViewModel;


/**
 * Created by Tar on 3/17/16 AD.
 */
public abstract class BaseViewModelActivity extends SSAActivity {

  BaseViewModel baseViewModel;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    initializeInjection(savedInstanceState);
    ViewDataBinding dataBinding = DataBindingUtil.setContentView(this, getViewResourceId());
    baseViewModel = getBaseViewModel();
    afterLoadContentView(dataBinding, savedInstanceState);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (baseViewModel != null) {
      baseViewModel.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (getBaseViewModel() != null) {
      getBaseViewModel().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  @Override
  protected void onStart() {
    super.onStart();

    if (baseViewModel != null) {
      baseViewModel.onStart();
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if (baseViewModel != null) {
      baseViewModel.onResume();
    }
  }

  @Override
  public void onPause() {
    super.onPause();

    if (baseViewModel != null) {
      baseViewModel.onPause();
    }
  }

  @Override
  protected void onStop() {
    super.onStop();

    if (baseViewModel != null) {
      baseViewModel.onStop();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    if (baseViewModel != null) {
      baseViewModel.onDestroy();
    }
  }

  public abstract int getViewResourceId();

  protected abstract void initializeInjection(Bundle savedInstanceState);

  protected abstract void afterLoadContentView(ViewDataBinding dataBinding, Bundle savedInstanceState);

  protected abstract BaseViewModel getBaseViewModel();
}
