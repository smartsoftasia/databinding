package com.smartsoftasia.library.databinding.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;


/**
 * Created by Tar on 3/17/16 AD.
 */
public abstract class BaseActivity extends SSAActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    beforeLoadContentView(savedInstanceState);
    setContentView(getViewResourceId());
    setActionbar();
    afterLoadContentView(savedInstanceState);
  }

  public abstract int getViewResourceId();

  protected void beforeLoadContentView(Bundle savedInstanceState) {

  }

  protected void afterLoadContentView(Bundle savedInstanceState) {

  }
}
