package com.smartsoftasia.library.databinding.view.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.smartsoftasia.library.databinding.helper.FontHelper;


/**
 * @author gregoire
 */
@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
public class BaseImageButton extends AppCompatImageButton  {

  private FontHelper.FontStyle mCurrentStyle;

  public BaseImageButton(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseImageButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }

  private void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }
  }

  @Override
  public void setBackground(Drawable background) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
      //noinspection deprecation
      super.setBackgroundDrawable(background);
    } else {
      super.setBackground(background);
    }
  }
}
