package com.smartsoftasia.library.databinding.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import com.smartsoftasia.library.databinding.view.activity.SSAActivity;


/**
 * Created by Tar on 11/8/16.
 */

public class SSAFragment extends Fragment {

  public void startCompactActivity(Class clazz) {
    startCompactActivity(clazz, null);
  }

  public void startCompactActivity(Class clazz, Bundle extras) {
    Intent intent = new Intent();
    intent.setClass(getActivity(), clazz);
    startCompactActivity(intent, extras, null);
  }

  public void startCompactActivity(Intent intent, Bundle extras, Bundle launchOptions) {
    if (extras != null) {
      intent.putExtras(extras);
    }

    ActivityCompat.startActivity(getActivity(), intent, launchOptions);
  }

  public void startNewStackActivity(Class clazz) {
    startNewStackActivity(clazz, null);
  }

  public void startNewStackActivity(Class clazz, Bundle extras) {
    Intent intent = new Intent();
    intent.setClass(getActivity(), clazz);
    startNewStackActivity(intent, extras, null);
  }

  public void startNewStackActivity(Intent intent, Bundle extras, Bundle launchOptions) {
    if (getActivity() == null || intent == null) {
      return;
    }

    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

    if (extras != null) {
      intent.putExtras(extras);
    }

    ActivityCompat.startActivity(getActivity(), intent, launchOptions);
  }

  protected void replaceFragment(int containerId, android.support.v4.app.Fragment fragment) {
    replaceFragment(containerId, fragment, null, false, 0, 0);
  }

  protected void replaceFragment(int containerId, android.support.v4.app.Fragment fragment, String tag,
                                 boolean addToBackStack, int enterAnim, int exitAnim) {
    android.support.v4.app.Fragment currentFragment = getChildFragmentManager().findFragmentById(containerId);

    if (currentFragment != null && (currentFragment.getClass().equals(fragment.getClass()))) {
      return;
    }

    FragmentTransaction ft = getChildFragmentManager().beginTransaction();

    if (enterAnim != 0 && exitAnim != 0) {
      ft.setCustomAnimations(enterAnim, exitAnim);
    }

    if (addToBackStack) {
      ft.addToBackStack(null);
    }

    ft.replace(containerId, fragment, tag);
    ft.commit();
  }

  protected void addFragment(int containerId, android.support.v4.app.Fragment fragment) {
    addFragment(containerId, fragment, null, false, 0, 0);
  }

  protected void addFragment(int containerId, android.support.v4.app.Fragment fragment, String tag, boolean addToBackStack,
                             int enterAnim, int exitAnim) {
    FragmentTransaction ft = getChildFragmentManager().beginTransaction();

    if (enterAnim != 0 && exitAnim != 0) {
      ft.setCustomAnimations(enterAnim, exitAnim);
    }

    if (addToBackStack) {
      ft.addToBackStack(null);
    }

    ft.add(containerId, fragment, tag);
    ft.commit();
  }

  protected void removeFragment(int containerId) {
    Fragment fragment = getChildFragmentManager().findFragmentById(containerId);
    removeFragment(fragment);
  }

  protected void removeFragment(Fragment fragment) {
    if (fragment == null) {
      return;
    }

    getChildFragmentManager().beginTransaction().remove(fragment).commit();
  }

  protected void setActionbarTitle(String title) {
    ActionBar actionBar = getBaseActivity().getSupportActionBar();

    if (actionBar != null) {
      actionBar.setTitle(title == null ? "" : title);
    }
  }

  protected void setActionbarTitle(int titleResourceId) {
    setActionbarTitle(getResources().getString(titleResourceId));
  }

  protected SSAActivity getBaseActivity() {
    return (SSAActivity) getActivity();
  }
}
