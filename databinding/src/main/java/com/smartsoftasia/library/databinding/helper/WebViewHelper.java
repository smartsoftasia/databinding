package com.smartsoftasia.library.databinding.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Nott on 25/11/2559.
 * dtm-iom-android
 */

public class WebViewHelper {
  public static final String TAG = "WebViewHelper";

  /**
   * AppWebViewClient
   * <p>
   * For when user click link is ongoing WebView instead launch browser
   */
  public static class AppWebViewClient extends WebViewClient {
    private Context context;

    public AppWebViewClient(Context context) {
      this.context = context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      return handleURL(view, url);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
      return handleURL(view, request.getUrl().toString());
    }

    private boolean handleURL(WebView webView, String url) {
      if (url.startsWith("http:") || url.startsWith("https:")) {
        webView.loadUrl(url);
      } else if (url.startsWith("tel:")) {
        Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
        context.startActivity(tel);
      } else if (url.startsWith("mailto:")) {
        Intent mail = new Intent(Intent.ACTION_SENDTO);
        mail.setData(Uri.parse(url));
        context.startActivity(mail);
      }
      return true;
    }
  }
}
