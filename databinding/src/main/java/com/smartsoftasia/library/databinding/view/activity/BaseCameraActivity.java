package com.smartsoftasia.library.databinding.view.activity;

import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;
import android.view.ViewGroup;

import com.smartsoftasia.library.databinding.view.widget.BaseCameraPreview;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Tar on 5/18/16 AD.
 */
public abstract class BaseCameraActivity extends BaseActivity implements Camera.PictureCallback {

  public static final String TAG = "BaseCameraActivity";

  public static final int MEDIA_TYPE_IMAGE = 1;
  public static final int MEDIA_TYPE_VIDEO = 2;

  protected Camera mCamera;
  protected BaseCameraPreview mCameraPreview;
  protected File pictureFile;

  public abstract BaseCameraPreview getCameraPreview(Camera camera);
  public abstract ViewGroup getPreviewView();
  public abstract void onCapturePhotoSuccess(File file);
  public abstract void onCapturePhotoFailed(Exception e);

  @Override
  protected void onResume() {
    super.onResume();

    if (hasCameraHardware()) {
      mCamera = getCameraInstance();
      mCameraPreview = getCameraPreview(mCamera);
      getPreviewView().addView(mCameraPreview);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();

    if (mCamera != null) {
      mCamera.release();
      mCamera = null;
      getPreviewView().removeView(mCameraPreview);
    }
  }

  @Override
  public void onPictureTaken(byte[] data, Camera camera) {
    pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
    if (pictureFile == null) {
      return;
    }

    try {
      FileOutputStream fos = new FileOutputStream(pictureFile);
      fos.write(data);
      fos.close();
      onCapturePhotoSuccess(pictureFile);
    } catch (Exception e) {
      pictureFile = null;
      onCapturePhotoFailed(e);
    }
  }

  protected boolean hasCameraHardware() {
    return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
  }

  protected void takePhoto() {
    try {
      mCamera.autoFocus(new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
          mCamera.cancelAutoFocus();

          if (success) {
            mCamera.takePicture(null, null, getPictureCallback());
          }
        }
      });
    } catch (Exception e) {
      restartPreview();
    }
  }

  protected void restartPreview() {
    pictureFile = null;
    mCamera.startPreview();
  }

  protected Camera.PictureCallback getPictureCallback() {
    return this;
  }

  public static Camera getCameraInstance() {
    Camera c = null;
    try {
      c = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
    } catch (Exception e) {
      Log.i(TAG, "Failed to open camera : " + e.getLocalizedMessage());
    }

    return c;
  }

  protected static File getOutputMediaFile(int type) {
    File mediaStorageDir = new File(
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
        ".tmp"
    );

    if (!mediaStorageDir.exists()) {
      if (!mediaStorageDir.mkdirs()) {
        return null;
      }
    }

    // Create a media file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    File mediaFile;
    if (type == MEDIA_TYPE_IMAGE) {
      mediaFile = new File(mediaStorageDir.getPath() + File.separator +
          "IMG_" + timeStamp + ".jpg");
    } else if (type == MEDIA_TYPE_VIDEO) {
      mediaFile = new File(mediaStorageDir.getPath() + File.separator +
          "VID_" + timeStamp + ".mp4");
    } else {
      return null;
    }

    return mediaFile;
  }
}
