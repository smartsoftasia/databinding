package com.smartsoftasia.library.databinding.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.IntRange;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;

public final class FileHelper {

  private static Context context;

  public static void init(Context _context) {
    context = _context;
  }

  private static File externalDirectoryPath(String... paths) {
    File externalFileDir = context.getExternalFilesDir(null);

    for (String path : paths) {
      externalFileDir = new File(externalFileDir, path);
    }

    return externalFileDir;
  }

  private static boolean outputStringToFile(String fileName, String value, int mode) {
    try {
      FileOutputStream fos = context.openFileOutput(fileName, mode);
      OutputStreamWriter osw = new OutputStreamWriter(fos);
      osw.write(value);
      osw.flush();
      osw.close();
    } catch (IOException e) {
      return false;
    }

    return true;
  }

  public static boolean appendStringToFile(String fileName, String value) {
    return outputStringToFile(fileName, value, Context.MODE_APPEND);
  }

  public static boolean writeStringToFile(String fileName, String value) {
    return outputStringToFile(fileName, value, Context.MODE_PRIVATE);
  }

  public static String readStringFromFile(String fileName) {
    StringBuffer sb = new StringBuffer();
    String inputLine = new String();

    try {
      FileInputStream fis = context.openFileInput(fileName);
      InputStreamReader isr = new InputStreamReader(fis);
      BufferedReader br = new BufferedReader(isr);

      while ((inputLine = br.readLine()) != null) {
        sb.append(inputLine);
        sb.append("\n");
      }

      br.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return sb.toString();
  }

  public static boolean fileExists(String... paths) {
    return externalDirectoryPath(paths).exists();
  }

  public static void deleteFile(String... paths) {
    File file = externalDirectoryPath(paths);
    context.deleteFile(file.getAbsolutePath());
  }

  public static File saveImageToFile(Context context, Bitmap image, String... paths)
      throws IOException {
    if (paths.length <= 0) {
      return null;
    }

    File file = externalDirectoryPath();
    for (int i = 0; i < paths.length; i++) {
      String path = paths[i];

      file = new File(file, path);

      if (i < paths.length - 1 && !file.exists()) {
        file.mkdirs();
      }
    }

    BufferedOutputStream buf = new BufferedOutputStream(new FileOutputStream(file));
    image.compress(Bitmap.CompressFormat.JPEG, 100, buf);
    buf.flush();
    buf.close();

    return file;
  }

  public static Bitmap getImageFromFile(String... paths) throws IOException {
    if (paths.length <= 0) {
      return null;
    }

    File file = externalDirectoryPath(paths);
    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));

    try {
      Bitmap image = BitmapFactory.decodeStream(bis);
      bis.close();
      return image;
    } catch (Exception e) {
      e.printStackTrace();
      bis.reset();
      return null;
    }
  }

  public static void writeSerializableObjectsToFile(ArrayList<Serializable> objects,
                                                    String fileName) throws IOException {
    FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
    ObjectOutput oo = new ObjectOutputStream(fos);
    oo.writeObject(objects);
    oo.close();
  }

  /**
   * compressImage from string file path
   *
   * @param path string path
   * @param percentImage percentage of size
   * @param compressFormat type of image
   * @return Resized File
   */
  public static File compressImage(String path, @IntRange(from = 0, to = 100) int percentImage,
                                   Bitmap.CompressFormat compressFormat) {
    File file = new File(path);
    Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bmp.compress(compressFormat, percentImage, bos);
    bmp.recycle();

    byte[] bytes = bos.toByteArray();

    try {
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(bytes);
      fos.flush();
      fos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return file;
  }

  /**
   * compressImage from Android Uri
   *
   * @param uri of image
   * @param percentImage percentage of size
   * @param compressFormat type of image
   * @return Resized File
   */
  public static File compressImage(Uri uri, @IntRange(from = 0, to = 100) int percentImage,
                                   Bitmap.CompressFormat compressFormat) {
    File file = new File(uri.getPath());
    Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bmp.compress(compressFormat, percentImage, bos);
    bmp.recycle();

    byte[] bytes = bos.toByteArray();

    try {
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(bytes);
      fos.flush();
      fos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return file;
  }

  /**
   * Resize image file
   *
   * @param file File object
   * @param percentImage percentage of size
   * @param compressFormat type of image
   */
  public static void compressImage(File file, @IntRange(from = 0, to = 100) int percentImage,
                                   Bitmap.CompressFormat compressFormat) {
    Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bmp.compress(compressFormat, percentImage, bos);
    bmp.recycle();

    byte[] bytes = bos.toByteArray();

    try {
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(bytes);
      fos.flush();
      fos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
