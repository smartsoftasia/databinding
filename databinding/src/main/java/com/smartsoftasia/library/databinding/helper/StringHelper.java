package com.smartsoftasia.library.databinding.helper;

import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by androiddev01 on 4/26/2016 AD.
 */
public class StringHelper {
  public static final String TAG = "StringHelper";

  /**
   * Test if a string contain at lease one of a element of the list
   * @param inputString string to test
   * @param items list string
   * @return boolean
   */
  public static boolean stringContainsItemFromList(String inputString, List<String> items) {
    if (inputString == null || items == null) return false;
    for(int i =0; i < items.size(); i++)
    {
      if(inputString.contains(items.get(i)))
      {
        return true;
      }
    }
    return false;
  }

  /**
   * return the substring form the index 0 to the first index of the character
   * @param s string to extract
   * @param c string to find the index
   * @return substring
   */
  @Nullable
  public static String substringIndexOf(@Nullable String s, @Nullable String c){
    if (s == null || c == null) return null;
    int index = s.indexOf(c);
    if (index>=0){
      return s.substring(0, index);
    }else{
      return s;
    }
  }

  /**
   * Remove all the special character from a string
   * @param s string to remove char
   * @return string without special char
   */
  @Nullable
  public static String removeSpecialChar(@Nullable String s){
    if (s==null)return null;
    return s.replaceAll("[^\\w]","");
  }
  /**
   * Return the first character to the index of the strings
   * @param s string to get the substring
   * @param length number of char to keep
   * @return chars of the string
   */
  public static String getFirstChar(@Nullable String s, int length){
    if (s == null || s.length() < length) return null;
    return s.substring(0, length);
  }

}
