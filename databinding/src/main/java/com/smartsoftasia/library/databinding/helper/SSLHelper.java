package com.smartsoftasia.library.databinding.helper;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Nott on 8/5/2016 AD.
 * AxaProvider
 */
public class SSLHelper {
  public static final String TAG = "SSLHelper";
  private static SSLHelper sSSLHelper;

  public static SSLHelper getSSLHelper() {
    if (sSSLHelper == null) {
      sSSLHelper = new SSLHelper();
    }
    return sSSLHelper;
  }

  /**
   * Provide SSL Socket Factory use on OkHttp for Trust all certificate
   *
   * @return SSL Socket Factory
   */
  public static SSLSocketFactory getSSLSocketFactoryForTrustAllCertificate() {
    SSLContext sslContext = null;
    try {
      sslContext = SSLContext.getInstance("TLS");
      sslContext.init(null, trustAllCerts, new SecureRandom());
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (KeyManagementException e) {
      e.printStackTrace();
    }
    return sslContext.getSocketFactory();
  }

  /**
   * Provide SSL Socket Context use on OkHttp for Trust all certificate
   *
   * @return SSL Socket Context
   */
  public static SSLContext getSSLContextForTrustAllCertificate() {
    SSLContext sslContext = null;
    try {
      sslContext = SSLContext.getInstance("TLS");
      sslContext.init(null, trustAllCerts, new SecureRandom());
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (KeyManagementException e) {
      e.printStackTrace();
    }
    return sslContext;
  }

  final static TrustManager[] trustAllCerts = new TrustManager[] {
      new X509TrustManager() {
        @Override
        public void checkClientTrusted(final X509Certificate[] chain, final String authType) {
        }

        @Override
        public void checkServerTrusted(final X509Certificate[] chain, final String authType) {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
          return new X509Certificate[0];
        }
      }
  };
}
