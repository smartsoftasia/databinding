package com.smartsoftasia.library.databinding.domain.executor;

import android.os.Handler;

/**
 * Created by gregoire barret on 5/13/15.
 * For Perfumist project.
 */
public interface UiThreadHandler {

    Handler getMainHandler();

    void post(Runnable r);
}
