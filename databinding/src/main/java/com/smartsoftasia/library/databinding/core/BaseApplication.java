package com.smartsoftasia.library.databinding.core;

import android.app.Application;

import com.smartsoftasia.library.databinding.R;
import com.smartsoftasia.library.databinding.helper.ColorHelper;
import com.smartsoftasia.library.databinding.helper.DeviceHelper;
import com.smartsoftasia.library.databinding.helper.FileHelper;
import com.smartsoftasia.library.databinding.helper.FontHelper;
import com.smartsoftasia.library.databinding.helper.TranslationHelper;

public abstract class BaseApplication extends Application implements AppStateMonitor.AppStateListener {

  protected static AppStateMonitor monitor;

  @Override
  public void onCreate() {
    super.onCreate();

    initializeInjector();

    monitor = AppStateMonitor.init(this);
    monitor.addListener(this);

    ColorHelper.init(this);
    DeviceHelper.init(this, getResources().getBoolean(R.bool.is_phone_factor));
    FileHelper.init(this);
    FontHelper.init(this);
    TranslationHelper.init(this);
  }

  @Override
  public void onBecameForeground() {

  }

  @Override
  public void onBecameBackground() {

  }

  public AppStateMonitor getMonitor(){
    return monitor;
  }

  protected abstract void initializeInjector();

}
