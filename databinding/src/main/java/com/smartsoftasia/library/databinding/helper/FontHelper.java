package com.smartsoftasia.library.databinding.helper;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.smartsoftasia.library.databinding.R;

import java.util.HashMap;

/**
 * Created by wolcan on 3/15/15.
 */
public class FontHelper {

  private static HashMap<FontStyle, Typeface> fontMap;
  private static Context context;
  private static FontStyle defaultFontStyle = FontStyle.REGULAR;

  public interface CustomFontWidget {
    void setCustomTypeface(Typeface typeface, float fontSizePercent);
  }

  public static void init(Context _context) {
    context = _context;

    fontMap = new HashMap<>();

    Resources res = context.getResources();
    loadFont(FontStyle.THIN, res.getString(R.string.default_font_thin));
    loadFont(FontStyle.LIGHT, res.getString(R.string.default_font_light));
    loadFont(FontStyle.REGULAR, res.getString(R.string.default_font_regular));
    loadFont(FontStyle.MEDIUM, res.getString(R.string.default_font_medium));
    loadFont(FontStyle.BOLD, res.getString(R.string.default_font_bold));
  }

  private static void loadFont(FontStyle style, String fontName) {
    if (fontMap == null) {
      fontMap = new HashMap<>();
    }

    if (fontName == null || fontName.isEmpty()) {
      return;
    }

    if (!fontName.startsWith("fonts/")) {
      fontName = "fonts/".concat(fontName);
    }

    fontMap.put(style, Typeface.createFromAsset(context.getAssets(), fontName));
  }

  public static void setTypeFace(CustomFontWidget view, AttributeSet attributeSet) {
    FontAttributes fa = new FontAttributes(context, attributeSet);

    if (fa.customFontName == null || fa.fontSizePercent <= 0) {
      return;
    }

    AssetManager mngr = context.getAssets();

    if (fa.customFontName != null) {
      view.setCustomTypeface(Typeface.createFromAsset(mngr, fa.customFontName), fa.fontSizePercent / 100.0f);
    } else if (fa.customFontStyle != FontStyle.NONE && fontMap.containsKey(fa.customFontStyle)) {
      view.setCustomTypeface(fontMap.get(fa.customFontStyle), fa.fontSizePercent / 100.0f);
    }
  }

  public static Typeface getCustomTypeface(FontStyle style) {
    return fontMap.get(style);
  }

  public static void setCustomTypeFace(FontStyle style, Typeface typeface) {
    fontMap.put(style, typeface);
  }

  public static void setDefaultFontStyle(FontStyle _defaultFontStyle) {
    defaultFontStyle = _defaultFontStyle;
  }

  public static class FontAttributes {
    protected String customFontName = null;
    protected FontStyle customFontStyle = null;
    protected Context context;
    protected int fontSizePercent = 100;

    public FontAttributes(Context context, String customFontName) {
      this.context = context;
      this.customFontName = customFontName;
    }

    public FontAttributes(Context context, AttributeSet attrs) {
      this.context = context;

      TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontAttributes);

      if (a != null) {
        customFontName = a.getString(R.styleable.FontAttributes_customFontName);

        if (customFontName != null && !customFontName.isEmpty() && !customFontName.startsWith("fonts/")) {
          customFontName = "fonts/".concat(customFontName);
        }

        fontSizePercent = a.getInt(R.styleable.FontAttributes_fontSizePercent, 100);

        int style = a.getInteger(R.styleable.FontAttributes_defaultFontStyle, -1);

        switch (style) {
          case 0:
            customFontStyle = FontStyle.NONE;
            break;
          case 1:
            customFontStyle = FontStyle.THIN;
            break;
          case 2:
            customFontStyle = FontStyle.LIGHT;
            break;
          case 3:
            customFontStyle = FontStyle.REGULAR;
            break;
          case 4:
            customFontStyle = FontStyle.MEDIUM;
            break;
          case 5:
            customFontStyle = FontStyle.BOLD;
            break;
          default:
            customFontStyle = defaultFontStyle;
            break;
        }

        a.recycle();
      }
    }
  }

  public enum FontStyle {
    THIN,
    LIGHT,
    REGULAR,
    MEDIUM,
    BOLD,
    NONE
  }
}
