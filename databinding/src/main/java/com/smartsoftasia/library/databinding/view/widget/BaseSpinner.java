package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by Nott on 7/11/2016 AD.
 * auto services
 */
public class BaseSpinner extends Spinner {
  public static final String TAG = "BaseSpinner";

  public BaseSpinner(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseSpinner(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public BaseSpinner(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes,
                     int mode) {
    super(context, attrs, defStyleAttr, defStyleRes, mode);
    init(context, attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.M)
  public BaseSpinner(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes,
                     int mode, Resources.Theme popupTheme) {
    super(context, attrs, defStyleAttr, defStyleRes, mode, popupTheme);
    init(context, attrs, defStyleAttr);
  }

  public BaseSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
    super(context, attrs, defStyleAttr, mode);
    init(context, attrs, defStyleAttr);
  }

  public BaseSpinner(Context context, int mode) {
    super(context, mode);
    init(context, null, 0);
  }

  private void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }
  }
}
