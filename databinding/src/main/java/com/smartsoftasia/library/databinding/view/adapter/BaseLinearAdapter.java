package com.smartsoftasia.library.databinding.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.List;

/**
 * Created by androiddev01 on 12/21/2015 AD.
 */
public abstract class BaseLinearAdapter<T> {
  public static final String TAG = "BaseLinearAdapter";

  protected List<T> items;
  protected HashMap<Integer, View> holder;

  protected Context context;
  protected OnItemClickListener itemClickListener;
  private LayoutInflater mInflater;
  private BaseLinearAdapterInterface mAdapterInterface;


  public BaseLinearAdapter(List<T> items, Context context) {
    this.items = items;
    this.context = context;
    this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    this.holder = new HashMap<>();


  }

  public int getCount() {
    return items.size();
  }

  public Object getItem(int position) {
    return items.get(position);
  }

  public long getItemId(int position) {
    return position;
  }

  public abstract View getView(final int position, View convertView, ViewGroup parent);

  public void setItems(List<T> items) {
    this.items.addAll(items);
    if (mAdapterInterface != null) mAdapterInterface.addItems();
  }

  public void setItem(T item) {
    this.items.add(item);
    if (mAdapterInterface != null) mAdapterInterface.addItem(0);
  }

  public void updateItem(T item, int position) {
    this.items.remove(position);
    this.items.add(position, item);

    if (mAdapterInterface != null) mAdapterInterface.addItems();
  }

  public void clear() {
    this.items.clear();
    this.holder.clear();
    if (mAdapterInterface != null) mAdapterInterface.deleteItems();
  }

  public LayoutInflater getInflater() {
    return mInflater;
  }

  public void setAdapterInterface(BaseLinearAdapterInterface adapterInterface) {
    mAdapterInterface = adapterInterface;
  }

  public void setItemClickListener(OnItemClickListener itemClickListener) {
    this.itemClickListener = itemClickListener;
  }

  public interface BaseLinearAdapterInterface {
    void addItem(int position);

    void addItems();

    void removeItem(int position);

    void deleteItems();
  }

  public interface OnItemClickListener {
    void onItemClick(int position);
  }
}
