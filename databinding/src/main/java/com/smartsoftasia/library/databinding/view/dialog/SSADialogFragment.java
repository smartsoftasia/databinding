package com.smartsoftasia.library.databinding.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.ViewGroup;
import android.view.Window;

import com.smartsoftasia.library.databinding.view.activity.SSAActivity;


/**
 * Created by Tar on 11/8/16.
 */

public class SSADialogFragment extends DialogFragment {

  protected boolean noTitle = true;
  protected boolean cancellable = true;
  protected boolean cancelOnTouchOutside = true;
  protected boolean fullscreen = false;
  protected boolean blurBackground = false;


  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    // request a window without the title
    if (noTitle) {
      dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    setCancelable(cancellable);
    setCancelOnTouchOutside(cancelOnTouchOutside);

    return dialog;
  }

  @Override
  public void onAttach(Context activity) {
    super.onAttach(activity);
  }

  @Override
  public void onResume() {
    super.onResume();

    Dialog dialog = getDialog();
    if (dialog != null && fullscreen) {
      int width = ViewGroup.LayoutParams.MATCH_PARENT;
      int height = ViewGroup.LayoutParams.MATCH_PARENT;
      dialog.getWindow().setLayout(width, height);
      dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
  }

  @Override
  public void onDismiss(DialogInterface dialog) {
    super.onDismiss(dialog);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }


  protected SSAActivity getBaseActivity() {
    return (SSAActivity) getActivity();
  }

  public void setCancellable(boolean cancellable) {
    this.cancellable = cancellable;
  }

  public void setCancelOnTouchOutside(boolean cancelOnTouchOutside) {
    this.cancelOnTouchOutside = cancelOnTouchOutside;
  }

  public void setNoTitle(boolean noTitle) {
    this.noTitle = noTitle;
  }

  public void setFullscreen(boolean fullscreen) {
    this.fullscreen = fullscreen;
  }

  public void setBlurBackground(boolean blurBackground) {
    this.blurBackground = blurBackground;
  }
}
