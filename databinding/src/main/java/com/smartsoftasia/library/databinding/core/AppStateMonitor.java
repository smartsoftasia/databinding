package com.smartsoftasia.library.databinding.core;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;

public class AppStateMonitor implements ActivityLifecycleCallbacks {

    public interface AppStateListener {

        public void onBecameForeground();

        public void onBecameBackground();
    }

    private static AppStateMonitor instance;

    private Activity topMostActivity;
    private boolean isForeground = false, isPause = true;
    private Handler handler = new Handler();
    private Runnable check;
    private ArrayList<AppStateListener> listeners = new ArrayList<AppStateListener>();

    public static AppStateMonitor init(Application app) {
        if (instance == null) {
            instance = new AppStateMonitor();
            app.registerActivityLifecycleCallbacks(instance);
        }

        return instance;
    }

    public static AppStateMonitor getInstance() {
        return instance;
    }

    private AppStateMonitor() {

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        boolean wasInBackground = !isForeground;
        isForeground = true;
        isPause = false;

        if (check != null) {
            handler.removeCallbacks(check);
        }

        if (wasInBackground) {
            notifyListener(true);
        }

        topMostActivity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isPause = true;

        if (check != null) {
            handler.removeCallbacks(check);
        }

        handler.postDelayed(check = new Runnable() {
            @Override
            public void run() {
                if (isForeground && isPause) {
                    isForeground = false;
                    notifyListener(false);
                }
            }
        }, 500);
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public Activity getTopMostActivity() {
        return topMostActivity;
    }

    public boolean isInForeground() {
        return isForeground;
    }

    public void addListener(AppStateListener listener) {
        listeners.add(listener);
    }

    public void removeListener(AppStateListener listener) {
        listeners.remove(listener);
    }

    private void notifyListener(boolean isInForeground) {
        for (AppStateListener listener : listeners) {
            try {
                if (isInForeground) {
                    listener.onBecameForeground();
                } else {
                    listener.onBecameBackground();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
