package com.smartsoftasia.library.databinding.bindingAdapter;

import android.databinding.BindingAdapter;
import android.support.v4.content.ContextCompat;

import com.smartsoftasia.library.databinding.view.widget.BaseTextView;


/**
 * Created by Tar on 11/16/16.
 */

public class TextViewBindingAdapter {

  @BindingAdapter("android:textColor")
  public static void setTextColor(BaseTextView baseTextView, int colorResource) {
    if (colorResource == 0) {
      return;
    }
    
    baseTextView.setTextColor(ContextCompat.getColor(baseTextView.getContext(), colorResource));
  }

  @BindingAdapter("android:text")
  public static void setText(BaseTextView baseTextView, int textResource) {
    baseTextView.setText(textResource);
  }

  @BindingAdapter("android:text")
  public static void setText(BaseTextView baseTextView, String text) {
    baseTextView.setText(text);
  }
}
