package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.smartsoftasia.library.databinding.R;
import com.smartsoftasia.library.databinding.helper.FontHelper;


/**
 * @author gregoire
 * @file TypedTexView.java, TypedTexView
 * @date Sep 29, 2014
 * @brief
 * @detail
 * @project vet
 */
public class BaseTextView extends AppCompatTextView implements FontHelper.CustomFontWidget {

  private boolean centeringText = false;
  private boolean adjustHeightToFitText = false;
  private Rect r = new Rect();

  public BaseTextView(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseTextView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }

  private void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }

    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TextViewAttributes);
    centeringText = a.getBoolean(R.styleable.TextViewAttributes_centeringText, false);
    adjustHeightToFitText = a.getBoolean(R.styleable.TextViewAttributes_adjustHeightToFitText, false);

    FontHelper.setTypeFace(this, attrs);
  }

  @Override
  public void onDraw(Canvas canvas) {
    if (centeringText) {
      drawCenter(canvas, getPaint(), getText().toString());
    } else {
      super.onDraw(canvas);
    }
  }

  private void drawCenter(Canvas canvas, Paint paint, String text) {
    canvas.getClipBounds(r);
    int cHeight = r.height();
    int cWidth = r.width();

    paint.setTextAlign(Paint.Align.LEFT);
    paint.getTextBounds(text, 0, text.length(), r);
    paint.setColor(getCurrentTextColor());
    paint.setTextSize(getTextSize());
    paint.setTypeface(getTypeface());

    float x = cWidth / 2f - r.width() / 2f - r.left;
    float y = cHeight / 2f + r.height() / 2f - r.bottom;
    canvas.drawText(text, x, y, paint);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    if (adjustHeightToFitText) {
      String text = getText().toString();
      getPaint().getTextBounds(text, 0, text.length(), r);
      int textHeight = (r.bottom - r.top);

      int height;
      int heightMode = MeasureSpec.getMode(heightMeasureSpec);
      int heightSize = MeasureSpec.getSize(heightMeasureSpec);

      if (heightMode == MeasureSpec.EXACTLY) {
        height = heightSize;
      } else if (heightMode == MeasureSpec.AT_MOST) {
        height = Math.min(textHeight, heightSize);
      } else {
        height = textHeight;
      }

      heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, heightMode);
    }

    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }

  @Override
  public void setCustomTypeface(Typeface typeface, float fontSizePercent) {
    int style = Typeface.NORMAL;

    if (getTypeface() != null) {
      style = getTypeface().getStyle();
    }

    setTypeface(typeface, style);
    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * fontSizePercent);
  }

  public void setCompoundDrawablesWithIntrinsicBounds(@DrawableRes int drawable) {
    setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getContext(), drawable), null, null, null);
  }

  public boolean isEmpty() {
    String ed_text = this.getText().toString().trim();
    return ed_text.isEmpty() || ed_text.length() == 0 || ed_text.equals("");
  }
}
