package com.smartsoftasia.library.databinding.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.smartsoftasia.library.databinding.helper.ColorHelper;
import com.smartsoftasia.library.databinding.helper.FontHelper;


/**
 * @author gregoire
 */
public class BaseButton extends AppCompatButton implements FontHelper.CustomFontWidget, ColorHelper.ImageTintColor {

  private FontHelper.FontStyle mCurrentStyle;

  public BaseButton(Context context) {
    super(context);
    init(context, null, 0);
  }

  public BaseButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs, 0);
  }

  public BaseButton(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr);
  }

  private void init(Context context, AttributeSet attrs, int defStyleAttr) {
    if (isInEditMode()) {
      return;
    }

    FontHelper.setTypeFace(this, attrs);
  }

  @Override
  public void setBackground(Drawable background) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
      //noinspection deprecation
      super.setBackgroundDrawable(background);
    } else {
      super.setBackground(background);
    }
  }

  @Override
  public void setCustomTypeface(Typeface typeface, float fontSizePercent) {
    int style = Typeface.NORMAL;

    if (getTypeface() != null) {
      style = getTypeface().getStyle();
    }

    setTypeface(typeface, style);
    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * fontSizePercent);
  }

  @Override
  public void setImageTintColor(int color) {
    for (Drawable drawable : getCompoundDrawables()) {
      drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }
  }
}
