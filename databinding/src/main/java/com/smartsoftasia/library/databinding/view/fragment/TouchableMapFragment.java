package com.smartsoftasia.library.databinding.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;
import com.smartsoftasia.library.databinding.view.widget.TouchableWrapper;

/**
 * Created by androiddev01 on 12/25/2015 AD.
 */
public class TouchableMapFragment extends SupportMapFragment {

  private View mOriginalContentView;
  private TouchableWrapper mTouchView;

  public void setTouchListener(TouchableWrapper.OnTouchListener onTouchListener) {
    mTouchView.setTouchListener(onTouchListener);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                           Bundle savedInstanceState) {
    mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);

    mTouchView = new TouchableWrapper(getActivity());
    mTouchView.addView(mOriginalContentView);

    return mTouchView;
  }

  @Override
  public View getView() {
    return mOriginalContentView;
  }
}
