package com.smartsoftasia.library.databinding.view.dialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartsoftasia.library.databinding.viewModel.BaseViewModel;


/**
 * Created by androiddev01 on 12/3/2015 AD.
 */
public abstract class BaseViewModelDialogFragment extends SSADialogFragment {

  BaseViewModel baseViewModel;

  public static final String TAG = "BaseDialogFragment";

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    initializeInjection(savedInstanceState);
    View view = inflater.inflate(getLayoutRes(), container, false);
    ViewDataBinding dataBinding = DataBindingUtil.bind(view);
    baseViewModel = getBaseViewModel();
    afterLoadContentView(dataBinding, savedInstanceState);

    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (baseViewModel != null) {
      baseViewModel.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  public void onStart() {
    super.onStart();

    if (baseViewModel != null) {
      baseViewModel.onStart();
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if (baseViewModel != null) {
      baseViewModel.onResume();
    }
  }

  @Override
  public void onPause() {
    super.onPause();

    if (baseViewModel != null) {
      baseViewModel.onPause();
    }
  }

  @Override
  public void onStop() {
    super.onStop();

    if (baseViewModel != null) {
      baseViewModel.onStop();
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();

    if (baseViewModel != null) {
      baseViewModel.onDestroy();
    }
  }

  protected abstract int getLayoutRes();

  protected abstract void initializeInjection(Bundle savedInstanceState);

  protected abstract void afterLoadContentView(ViewDataBinding dataBinding, Bundle savedInstanceState);

  protected abstract BaseViewModel getBaseViewModel();
}
