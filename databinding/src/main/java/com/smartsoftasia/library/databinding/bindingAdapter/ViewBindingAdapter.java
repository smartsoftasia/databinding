package com.smartsoftasia.library.databinding.bindingAdapter;

import android.databinding.BindingAdapter;
import android.view.View;

/**
 * Created by Tar on 11/16/16.
 */

public class ViewBindingAdapter {

  @BindingAdapter("android:background")
  public static void setBackgroundDrawable(View view, int backgroundResource) {
    view.setBackgroundResource(backgroundResource);
  }

}
