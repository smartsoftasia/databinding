package com.smartsoftasia.library.databinding.bindingAdapter;

import android.databinding.BindingAdapter;
import android.net.Uri;

import com.bumptech.glide.DrawableRequestBuilder;
import com.smartsoftasia.library.databinding.view.widget.BaseImageView;
import com.smartsoftasia.library.databinding.view.widget.BaseRoundedImageView;

/**
 * Created by Tar on 11/16/16.
 */

public class ImageViewBindingAdapter {

  @BindingAdapter("imageFileUri")
  public static void setImageFromUri(BaseImageView imageView, Uri uri) {
    imageView.setImageURI(uri);
  }

  @BindingAdapter("android:src")
  public static void setImageResource(BaseImageView imageView, Uri uri) {
    imageView.setImageURI(uri);
  }

  @BindingAdapter("android:src")
  public static void setImageResource(BaseImageView imageView, int imageResource) {
    imageView.setImageResource(imageResource);
  }

  @BindingAdapter("android:src")
  public static void downloadImage(BaseImageView imageView, String imageUrl) {
    imageView.downloadImage(imageView.getContext(), imageUrl, null);
  }

  @BindingAdapter("imageFileUri")
  public static void setImageFromUri(BaseRoundedImageView imageView, Uri uri) {
    imageView.loadUri(imageView.getContext(), uri, new BaseImageView.LoadUriCallback() {
      @Override
      public DrawableRequestBuilder<Uri> onBuild(DrawableRequestBuilder<Uri> drb) {
        return drb.centerCrop();
      }
    });
  }

  @BindingAdapter("android:src")
  public static void downloadImage(BaseRoundedImageView imageView, String imageUrl) {
    imageView.downloadImage(imageView.getContext(), imageUrl, new BaseImageView.DownloadImageCallback() {
      @Override
      public DrawableRequestBuilder<String> onBuild(DrawableRequestBuilder<String> drb) {
        return drb.centerCrop();
      }
    });
  }

}
