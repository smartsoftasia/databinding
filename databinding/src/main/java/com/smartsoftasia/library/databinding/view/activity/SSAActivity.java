package com.smartsoftasia.library.databinding.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.smartsoftasia.library.databinding.R;
import com.smartsoftasia.library.databinding.helper.Validator;


/**
 * Created by Tar on 11/8/16.
 */

public class SSAActivity extends AppCompatActivity {

  protected ProgressDialog mProgressDialog;
  protected ActionBarDrawerToggle actionBarDrawerToggle;

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);

    if (actionBarDrawerToggle != null) {
      actionBarDrawerToggle.syncState();
    }
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);

    if (actionBarDrawerToggle != null) {
      actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (actionBarDrawerToggle != null && actionBarDrawerToggle.onOptionsItemSelected(item)) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  protected void setupDrawer() {
    if (getDrawerLayout() == null) {
      return;
    }

    actionBarDrawerToggle = new ActionBarDrawerToggle(
        this,
        getDrawerLayout(),
        getToolBar(),
        getDrawerOpenResource(),
        getDrawerCloseResource()) {
      @Override
      public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);

        onNavigationDrawerOpened(drawerView);
      }

      @Override
      public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);

        onNavigationDrawerClosed(drawerView);
      }

      @Override
      public void onDrawerSlide(View drawerView, float slideOffset) {
        super.onDrawerSlide(drawerView, slideOffset);

        onNavigationDrawerSlide(drawerView, slideOffset);
      }

      @Override
      public void onDrawerStateChanged(int newState) {
        super.onDrawerStateChanged(newState);

        onNavigationDrawerStateChanged(newState);
      }
    };
    getDrawerLayout().addDrawerListener(actionBarDrawerToggle);

    if (getSupportActionBar() != null) {
      getSupportActionBar().setHomeButtonEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    actionBarDrawerToggle.syncState();
  }

  protected int getDrawerOpenResource() {
    return R.string.navigation_drawer_open;
  }

  protected int getDrawerCloseResource() {
    return R.string.navigation_drawer_close;
  }

  protected DrawerLayout getDrawerLayout() {
    return null;
  }

  protected void onNavigationDrawerClosed(View drawerView) {

  }

  protected void onNavigationDrawerOpened(View drawerView) {

  }

  protected void onNavigationDrawerSlide(View drawerView, float slideOffset) {

  }

  protected void onNavigationDrawerStateChanged(int newState) {

  }

  public void startCompactActivity(Class clazz) {
    startCompactActivity(clazz, null);
  }

  public void startCompactActivity(Class clazz, Bundle extras) {
    Intent intent = new Intent();
    intent.setClass(this, clazz);
    startCompactActivity(intent, extras, null);
  }

  public void startCompactActivity(Intent intent, Bundle extras, Bundle launchOptions) {
    if (extras != null) {
      intent.putExtras(extras);
    }

    ContextCompat.startActivity(this, intent, launchOptions);
  }

  public void startNewStackActivity(Class clazz) {
    startNewStackActivity(clazz, null);
  }

  public void startNewStackActivity(Class clazz, Bundle extras) {
    Intent intent = new Intent();
    intent.setClass(this, clazz);
    startNewStackActivity(intent, extras, null);
  }

  public void startNewStackActivity(Intent intent, Bundle extras, Bundle launchOptions) {
    if (intent == null) {
      return;
    }

    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

    if (extras != null) {
      intent.putExtras(extras);
    }

    ActivityCompat.startActivity(this, intent, launchOptions);
  }

  public void showSnackBarError(String message) {
    View snackbarView = findViewById(android.R.id.content);
    if (snackbarView != null) {
      Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_LONG);
      snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.red));
      snackbar.show();
    } else {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
  }

  public void showSnackBarError(Throwable e) {
    View snackbarView = findViewById(android.R.id.content);
    String message = getString(R.string.error_title);
    if (e != null && Validator.isValid(e.getMessage())) {
      message = e.getMessage();
    }
    if (snackbarView != null) {
      Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_LONG);
      snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.red));
      snackbar.show();
    } else {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
  }

  /**
   * Recreate Activity
   */
  public void recreateActivity() {
    Intent intent = getIntent();
    finish();
    startActivity(intent);
  }

  public void showUndoSnackBar(String message, View.OnClickListener listener) {
    View snackBarInfo = findViewById(android.R.id.content);
    if (snackBarInfo != null) {
      Snackbar.make(snackBarInfo, message, Snackbar.LENGTH_LONG)
          .setAction(R.string.snackbar_undo, listener)
          .show();
    }
  }

  public void showSnackBartInfo(String message) {
    View snackBarInfo = findViewById(android.R.id.content);
    if (snackBarInfo != null) {
      Snackbar.make(snackBarInfo, message, Snackbar.LENGTH_LONG).show();
    } else {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
  }

  public void showToastError(String message) {
    View snackbarView = findViewById(android.R.id.content);
    if (snackbarView != null) {
      Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_LONG);
      snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.red));
      snackbar.show();
    } else {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
  }

  public void showToastError(Throwable e) {
    View snackbarView = findViewById(android.R.id.content);
    String message = getString(R.string.error_title);
    if (e != null && Validator.isValid(e.getMessage())) {
      message = e.getMessage();
    }
    if (snackbarView != null) {
      Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_LONG);
      snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.red));
      snackbar.show();
    } else {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
  }

  public void showToastInfo(String message) {
    View snackBarInfo = findViewById(android.R.id.content);
    if (snackBarInfo != null) {
      Snackbar.make(snackBarInfo, message, Snackbar.LENGTH_LONG).show();
    } else {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
  }

  protected void replaceFragment(int containerId, Fragment fragment) {
    replaceFragment(containerId, fragment, null, false, 0, 0, false);
  }

  protected void replaceFragment(int containerId, Fragment fragment, boolean replaceAnyway) {
    replaceFragment(containerId, fragment, null, false, 0, 0, replaceAnyway);
  }

  protected void replaceFragment(int containerId, Fragment fragment, String tag,
                                 boolean addToBackStack, int enterAnim, int exitAnim,
                                 boolean replaceAnyway) {
    Fragment currentFragment = getSupportFragmentManager().findFragmentById(containerId);

    if (!replaceAnyway) {
      if (currentFragment != null && (currentFragment.getClass().equals(fragment.getClass()))) {
        return;
      }
    }

    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

    if (enterAnim != 0 && exitAnim != 0) {
      ft.setCustomAnimations(enterAnim, exitAnim);
    }

    if (addToBackStack) {
      ft.addToBackStack(null);
    }

    ft.replace(containerId, fragment, tag);
    ft.commit();
  }

  protected void addFragment(int containerId, Fragment fragment) {
    addFragment(containerId, fragment, null, false, 0, 0);
  }

  protected void addFragment(int containerId, Fragment fragment, String tag, boolean addToBackStack,
                             int enterAnim, int exitAnim) {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

    if (enterAnim != 0 && exitAnim != 0) {
      ft.setCustomAnimations(enterAnim, exitAnim);
    }

    if (addToBackStack) {
      ft.addToBackStack(null);
    }

    ft.add(containerId, fragment, tag);
    ft.commit();
  }

  protected void removeFragment(int containerId) {
    Fragment fragment = getSupportFragmentManager().findFragmentById(containerId);
    removeFragment(fragment);
  }

  protected void removeFragment(Fragment fragment) {
    if (fragment == null) {
      return;
    }

    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
  }

  public void hideKeyboard() {
    View view = getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }

  protected void setActionbar() {
    setSupportActionBar(getToolBar());
  }

  protected void setActionbarNoTitle() {
    if (getSupportActionBar() == null) {
      setActionbar();
    }

    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
  }

  protected void setActionbarTitle(String title) {
    if (getSupportActionBar() == null) {
      setActionbar();
    }

    if (getSupportActionBar() != null) {
      getSupportActionBar().setTitle(title == null ? "" : title);
    }
  }

  protected void setActionbarTitle(int titleResourceId) {
    setActionbarTitle(getResources().getString(titleResourceId));
  }

  protected void setBackOnActionbar() {
    showBackOnActionBar();
  }

  protected void showBackOnActionBar() {
    if (getSupportActionBar() == null) {
      setActionbar();
    }

    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
  }

  protected void hideBackOnActionBar() {
    if (getSupportActionBar() == null) {
      setActionbar();
    }

    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
  }

  protected void setActionBarCustomView(View v) {
    if (getSupportActionBar() == null) {
      setActionbar();

      if (getSupportActionBar() == null) {
        return;
      }
    }

    ActionBar actionBar = getSupportActionBar();
    actionBar.setCustomView(v);
    actionBar.setDisplayUseLogoEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(false);
    actionBar.setDisplayShowHomeEnabled(false);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayShowCustomEnabled(true);

    ViewParent parent = v.getParent();
    if (parent instanceof Toolbar) {
      ((Toolbar) parent).setContentInsetsAbsolute(0, 0);
    }
  }

  @Nullable
  protected Toolbar getToolBar() {
    return (Toolbar) findViewById(getToolbarId());
  }

  public int getToolbarId() {
    return R.id.toolbar;
  }

  public void openEmail() {
    Intent intent = new Intent(Intent.ACTION_SEND);
    intent.setType("message/rfc822");
    intent.putExtra(Intent.EXTRA_EMAIL, "");
    startActivity(Intent.createChooser(intent, ""));
  }

  public void displayImageChooser(final OnSelectImageChooserListener listener) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setItems(getResources().getStringArray(R.array.picture_select_array),
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            switch (which) {
              case 0:
                if (listener != null) listener.onGallerySelected();
                break;
              case 1:
                if (listener != null) listener.onCameraSelected();
                break;
            }
          }
        });
    builder.show();
  }

  public boolean isPermissionGranted(String permission) {
    if (Build.VERSION.SDK_INT >= 23) {
      int result = ContextCompat.checkSelfPermission(this, permission);

      return result == PackageManager.PERMISSION_GRANTED;
    } else {
      return true;
    }
  }

  public void requestPermission(String permission, int permissionRequestCode) {
    if (Build.VERSION.SDK_INT >= 23) {
      requestPermissions(new String[] { permission }, permissionRequestCode);
    }
  }

  public void displayLoadingDialog(String s) {
    hideLoadingDialog();
    mProgressDialog = new ProgressDialog(this); // this = YourActivity
    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    mProgressDialog.setMessage(s);
    mProgressDialog.setIndeterminate(true);
    mProgressDialog.setCanceledOnTouchOutside(false);
    mProgressDialog.show();
  }

  public void hideLoadingDialog() {
    if (mProgressDialog != null) {
      mProgressDialog.dismiss();
    }
  }

  public interface OnSelectImageChooserListener {
    void onGallerySelected();

    void onCameraSelected();
  }
}
