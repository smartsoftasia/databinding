package com.smartsoftasia.library.databinding.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by Tar on 7/21/16 AD.
 */
public class MaintainPositionListView extends ListView {

  public MaintainPositionListView(Context context) {
    super(context);
  }

  public MaintainPositionListView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public MaintainPositionListView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  public void setAdapter(ListAdapter adapter) {
    int index = getFirstVisiblePosition();
    View v = getChildAt(0);
    int top = (v == null) ? 0 : (v.getTop() - getPaddingTop());

    super.setAdapter(adapter);

    setSelectionFromTop(index, top);
  }
}
