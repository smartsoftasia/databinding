package com.smartsoftasia.library.databinding.view.adapter;

import java.util.Collection;
import java.util.List;

/**
 * Created by Hinda on 25/02/16.
 */
public interface RecycleAdapter<T> {


  Object getItem(int position);

  List<T> getItems();

  void appendItems(Collection<T> items);

  void appendItem(T item);

  void appendNewsItems(Collection<T> items);

  void clear();

  void deleteItem(int position);

  void deleteItem(T item);

  void addItem(int position, T item);
}
