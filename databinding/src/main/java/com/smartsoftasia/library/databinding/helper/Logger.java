package com.smartsoftasia.library.databinding.helper;

import android.util.Log;

/**
 * @author androiddev01 in 11/11/2015 AD.
 *
 *         This class send a {@link Log} message only if the buildConf is on DEBUG
 */
public class Logger {

  private static boolean DEBUG = false;

  public static void init(boolean debug) {
    DEBUG = debug;
  }

  /**
   * Send a {@link Log#DEBUG} log message.
   *
   * @param tag Used to identify the source of a log message.  It usually identifies
   * the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static void d(String tag, String msg) {
    if (DEBUG) {
      Log.d(tag, msg);
    }
  }

  /**
   * Send an {@link Log#INFO} log message.
   *
   * @param tag Used to identify the source of a log message.  It usually identifies
   * the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static void i(String tag, String msg) {
    if (DEBUG) {
      Log.i(tag, msg);
    }
  }

  /**
   * Send an {@link Log#ERROR} log message.
   *
   * @param tag Used to identify the source of a log message.  It usually identifies
   * the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static void e(String tag, String msg) {
    if (DEBUG) {
      Log.e(tag, msg);
    }
  }

  /**
   * Send an {@link Log#VERBOSE} log message.
   *
   * @param tag Used to identify the source of a log message.  It usually identifies
   * the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static void v(String tag, String msg) {
    if (DEBUG) {
      Log.v(tag, msg);
    }
  }

  /**
   * Send an {@link Log#WARN} log message.
   *
   * @param tag Used to identify the source of a log message.  It usually identifies
   * the class or activity where the log call occurs.
   * @param msg The message you would like logged.
   */
  public static void w(String tag, String msg) {
    if (DEBUG) {
      Log.w(tag, msg);
    }
  }
}
