package com.smartsoftasia.library.databinding.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by androiddev01 on 11/30/2015 AD.
 */
public abstract class BaseAdapter<T> extends android.widget.BaseAdapter {
  public static final String TAG = "AbstractAdapter";

  protected List<T> items;

  private Context mContext;
  private LayoutInflater mInflater;

  public BaseAdapter(Context context, List<T> items) {
    this.mContext = context;
    this.items = items;
    this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return items.size();
  }

  @Override
  public Object getItem(int i) {
    return items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    return null;
  }

  public List<T> getItems() {
    return items;
  }

  public void setItems(List<T> items) {
    this.items.addAll(items);
    notifyDataSetChanged();
  }

  public void setItem(T item) {
    this.items.add(item);
    notifyDataSetChanged();
  }

  public void setItem(T item, int position) {
    this.items.remove(position);
    this.items.add(position, item);
    notifyDataSetChanged();
  }

  public void setItemsClean(List<T> items) {
    this.items.clear();
    setItems(items);
  }

  public void clear() {
    this.items.clear();
    notifyDataSetChanged();
  }

  public Context getContext() {
    return mContext;
  }

  public LayoutInflater getInflater() {
    return mInflater;
  }
}
