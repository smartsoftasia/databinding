package com.smartsoftasia.library.databinding.viewModel;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.os.Bundle;

/**
 * Created by Tar on 11/8/16.
 */

public abstract class BaseViewModel extends BaseObservable implements BaseViewModelInterface {

  protected ViewModelState state;

  @Override
  public void onStart() {
    state = ViewModelState.START;
  }

  @Override
  public void onResume() {
    state = ViewModelState.RESUME;
  }

  @Override
  public void onPause() {
    state = ViewModelState.PAUSE;
  }

  @Override
  public void onStop() {
    state = ViewModelState.STOP;
  }

  @Override
  public void onDestroy() {
    state = ViewModelState.DESTROY;
  }

  public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

  }

  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

  }

  public void startActivity(Context context, Class activityClass) {
    startActivity(context, activityClass, false);
  }

  public void startActivity(Context context, Class activityClass, final boolean clear) {
    startActivity(context, activityClass, null, clear);
  }

  public void startActivity(Context context, Class activityClass, final Bundle extras, final boolean clear) {
    startActivity(context, activityClass, new ProcessIntentCallback() {
      @Override
      public Intent onCreateIntent(Intent intent) {
        if (clear) {
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }

        if (extras != null) {
          intent.putExtras(extras);
        }

        return intent;
      }
    });
  }

  public void startActivity(Context context, Class activityClass, ProcessIntentCallback callback) {
    Intent intent = new Intent(context, activityClass);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    if (callback != null) {
      intent = callback.onCreateIntent(intent);
    }

    context.startActivity(intent);
  }

  public boolean isPermitToShowFragment() {
    return state == ViewModelState.START || state == ViewModelState.RESUME;
  }

  public interface ProcessIntentCallback {
    Intent onCreateIntent(Intent intent);
  }

  public enum ViewModelState {
    START,
    RESUME,
    PAUSE,
    STOP,
    DESTROY
  }
}
