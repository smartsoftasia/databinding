package com.smartsoftasia.library.databinding.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.Collection;
import java.util.List;

/**
 * Created by androiddev01 on 11/30/2015 AD.
 */
public abstract class BaseRecyclerView<VH extends RecyclerView.ViewHolder, T>
    extends RecyclerView.Adapter<VH> implements RecycleAdapter<T> {
  public static final String TAG = "AbstractRecyclerView";

  protected List<T> items;
  protected Context context;
  protected OnAdapterClick adapterClickListener;
  protected OnAdapterLongClick adapterLongClick;

  public BaseRecyclerView(List<T> items, Context context) {
    this.items = items;
    this.context = context;
  }

  @Override
  public Object getItem(int position) {
    return items.get(position);
  }

  @Override
  public List<T> getItems() {
    return items;
  }

  @Override
  public void appendItems(Collection<T> items) {
    this.items.addAll(items);
    this.notifyDataSetChanged();
  }

  @Override
  public void appendItem(T item) {
    this.items.add(item);
    this.notifyDataSetChanged();
  }

  @Override
  public void clear() {
    this.items.clear();
    this.notifyDataSetChanged();
  }

  @Override
  public void deleteItem(int position) {
    this.items.remove(position);
    this.notifyItemRemoved(position);
    this.notifyItemRangeChanged(position, getItemCount());
  }

  @Override
  public void deleteItem(T item) {
    int position = this.items.indexOf(item);
    if (position > -1){
      this.items.remove(position);
      this.notifyItemRemoved(position);
      this.notifyItemRangeChanged(position, getItemCount());
    }
  }

  @Override
  public int getItemCount() {
    return this.items.size();
  }

  @Override
  public void appendNewsItems(Collection<T> items) {
    this.items.clear();
    this.appendItems(items);
  }

  @Override
  public void addItem(int position, T item) {
    this.items.add(position, item);
    this.notifyItemInserted(position);
  }

  public void changeItem(int position, T item) {
    this.items.set(position, item);
    this.notifyItemChanged(position);
  }

  public void setAdapterClickListener(OnAdapterClick adapterClickListener) {
    this.adapterClickListener = adapterClickListener;
  }

  public void setAdapterLongClick(OnAdapterLongClick adapterLongClick) {
    this.adapterLongClick = adapterLongClick;
  }
}
