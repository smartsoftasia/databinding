package com.smartsoftasia.library.databinding.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartsoftasia.library.databinding.viewModel.BaseViewModel;


/**
 * Created by Tar on 3/17/16 AD.
 * BaseFragment
 */
public abstract class BaseViewModelFragment extends SSAFragment {

  BaseViewModel baseViewModel;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    initializeInjection(savedInstanceState);
    View view = inflater.inflate(getViewResourceId(), container, false);
    ViewDataBinding dataBinding = DataBindingUtil.bind(view);
    baseViewModel = getBaseViewModel();
    afterLoadContentView(dataBinding, savedInstanceState);

    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (baseViewModel != null) {
      baseViewModel.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (getBaseViewModel() != null) {
      getBaseViewModel().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  @Override
  public void onStart() {
    super.onStart();

    if (baseViewModel != null) {
      baseViewModel.onStart();
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if (baseViewModel != null) {
      baseViewModel.onResume();
    }
  }

  @Override
  public void onPause() {
    super.onPause();

    if (baseViewModel != null) {
      baseViewModel.onPause();
    }
  }

  @Override
  public void onStop() {
    super.onStop();

    if (baseViewModel != null) {
      baseViewModel.onStop();
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();

    baseViewModel.onDestroy();
  }

  public abstract int getViewResourceId();

  protected abstract void initializeInjection(Bundle savedInstanceState);

  protected abstract void afterLoadContentView(ViewDataBinding dataBinding, Bundle savedInstanceState);

  protected abstract BaseViewModel getBaseViewModel();
}
